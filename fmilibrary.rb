class Fmilibrary < Formula
  desc "FMI Library is an independent open-source implementation of the FMI open standard (www.fmi-standard.org)"
  url "https://svn.jmodelica.org/FMILibrary/tags/2.0.3", :using => :svn

  depends_on "cmake" => :build

  def install
    mkdir "build"
    cd "build" do
      system "cmake -DFMILIB_INSTALL_PREFIX=#{opt_prefix} .."
      system "cmake --build ."
      system "make install"
    end
  end
end
